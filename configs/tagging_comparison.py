from plot_ft_contours import omega

# The first dict in the file will be used to fill the contour plot
TAGGING_COMPARISON = {
    # R. Aaij et al. (The LHCb collaboration). ‘Measurement of 𝐶𝑃 violation in 𝐵0 → 𝐷+𝐷− decays’. In: (Aug. 2016). arXiv: 1608.06620.
    'LHCb_B2DD': {
        'label': 'LHCb $B^0\\to D^+D^-$',
        'omega': 0.348,
        'eff': 0.876,
        'offset': (0.015, 0),
        'contour': True,
        'ref': 'LHCB-PAPER-2016-037',
    },
    # ATLAS Collaboration, Measurement of the CP violation phase φs in Bs → J/ψφ decays in ATLAS at 13 TeV, ATLAS-CONF-2019-009, CERN, Geneva, 2019.
    'ATLAS_Bs2JpsiPhi': {
        'label': 'ATLAS $B_\\mathrm{s}^0\\to J\\!/\\!\\psi \\phi$',
        'omega': omega(efficiency=0.2123, tagging_power=0.0175),
        'eff': 0.2123,
        'offset': (0.015, 0),
        'contour': (0.33, 0.15),
        'ref': 'atlas-bs2jpsiphi-2020',
    },
    # CMS collaboration, V. Khachatryan et al., Measurement of the CP-violating weak phase φS and the decay width difference ∆ΓS using the Bs 0 → J/ψφ(1020 ) decay channel in √pp collisions at s = 8 TeV, Physics Letters B 757 (2016) 97–120.
    'CMS_Bs2JpsiPhi': {
        'label': 'CMS $B_\\mathrm{s}^0\\to J\\!/\\!\\psi \\phi$',
        'omega': 0.3017,
        'eff': 0.0831,
        'ref': 'cms-bs2jpsiphi',
    },
    # B. Aubert et al. (The BaBar collaboration). ‘Measurement of time- dependent 𝐶𝑃 asymmetry in 𝐵0 → 𝑐𝑐𝐾(∗)0 decays’. In: Phys. Rev. D 79 (Apr. 2009), p. 072009. doi: 10.1103/PhysRevD.79.072009.
    'BaBar_Bd2ccKst': {
        'label': r'BaBar $B^0\to c\overline{c} K^{(*)0}$',
        'omega': 0.1761466374987154,
        'eff': 0.7437,
        'offset': (0.015, 0),
        'contour': True,
        'ref': 'babar',
    },
    # H. Kakuno et al. (The Belle collaboration). ‘Neutral flavor tagging for the measurement of mixing-induced 𝐶𝑃 violation at Belle’. In: Nu- clear Instruments and Methods in Physics Research Section A: Accelera- tors, Spectrometers, Detectors and Associated Equipment 533.3 (2004), pp. 516–531. doi: 10.1016/j.nima.2004.06.159.
    'Belle_sin2phi_1': {
        'label': r'Belle $B^0 \to c\overline{c} K^{(*)0}$',
        'omega': 0.2314031113784929,
        'eff': 0.998,
        'offset': (-0.05, -0.04),
        'ref': 'belle',
    },
    # R. Aaij et al. (The LHCb collaboration). ‘Measurement of 𝐶𝑃 Violation in 𝐵0 → 𝐽/𝜓𝐾S0 Decays’. In: Phys. Rev. Lett. 115 (July 2015), p. 031601. doi: 10.1103/PhysRevLett.115.031601.
    'LHCb_Bd2JpsiKS': {
        'label': r'LHCb $B^0\to J\!/\!\psi K_\mathrm{S}^0$',
        'omega': 0.3562,
        'eff': 0.3654,
        'offset': (0.015, 0),
        'contour': (0.38, 0.47),
        'ref': 'LHCb-PAPER-2015-004',
    },
    # LHCb collaboration, R. Aaij et al., Updated measurement of time-dependent CP- violating observables in Bs0 → J/ψK+K− decays, Eur. Phys. J. C79 (2019) 706, arXiv:1906.08356.
    'LHCb_Bs2JpsiPhi': {
        'label': 'LHCb $B_s^0\\to J\\!/\\!\\psi \\phi$',
        'omega': omega(dilution_squared=0.061),
        'eff': 0.778,
        'offset': (0.015, 0),
        'ref': 'LHCb-PAPER-2019-013',
    },
    'contours': {
        0.15: (0.15, 0.2),
        0.55: (0.05, 0.7),
        0.8: (0.02, 0.9),
    },
}
