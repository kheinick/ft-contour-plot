from plot_ft_contours import omega

# The first dict in the file will be used to fill the contour plot
LHCB_COMPARISON = {
    'LHCb_B2DstD_Run2': {  # LHCb-PAPER-2019-036, 1912.03723
        'omega': omega(efficiency=0.935, tagging_power=0.0661),
        'eff': 0.935,
    },
    'LHCb_B2DstD_Run2_Run1Data': {  # LHCb-PAPER-2019-036, 1912.03723
        'omega': omega(efficiency=0.802, tagging_power=0.0561),
        'eff': 0.802,
    },
    'LHCb_Bs2PhiPhi': {  # LHCb-PAPER-2019-019, 1907.10003
        'omega': omega(efficiency=0.768, tagging_power=0.0574),
        'eff': 0.768,
    },
    'LHCb_Bs2JpsiKK_2019': { # LHCb-PAPER-2019-013, 1906.08356
        'label': 'LHCb $B_s^0\\to J\\!/\\!\\psi K^+K^-$ 2019',
        'omega': omega(dilution_squared=0.061),
        'eff': 0.778,
    },
    'LHCb_Bs2PhiGamma': { # LHCb-PAPER-2019-015, 1905.06284
        'omega': omega(dilution=0.259, efficiency=0.745),
        'eff': 0.745,
    },
    'LHCb_Bs2JpsiPiPi': { # LHCb-PAPER-2019-003, 1903.05530
        'omega': omega(tagging_power=0.0506, efficiency=0.785),
        'eff': 0.785,
    },
    'LHCb_BsKPiKPi': { # LHCb-PAPER-2017-048, 1712.08683
        'omega': omega(tagging_power=0.0515, efficiency=0.756),
        'eff': 0.756,
    },
    'LHCb_BsKPiKPi': { # LHCb-PAPER-2017-029, 1709.03944
        'omega': omega(tagging_power=0.0515, efficiency=0.756),
        'eff': 0.756,
    },
    'LHCb_Bd2Jpsi(ee)KS': { # LHCb-PAPER-2017-029, 1709.03944
                            # combined efficiency not reported, taking largest
                            # reported single (SS) efficiency
        'omega': omega(tagging_power=0.0593, efficiency=0.759),
        'eff': 0.759,  # not reported in paper, taken from http://dx.doi.org/10.17877/DE290R-19128
        'contour': True,
    },
    'LHCb_Bd2Psi2SKS': { # LHCb-PAPER-2017-029, 1709.03944
        'omega': omega(tagging_power=0.0342, efficiency=0.4),
        'eff': 0.7,  # not reported in paper, nor in ana, assuming efficiency
                     # of charmonium measurement Bd2JpsiKS
        'contour': True,
    },
    'LHCb_Bs2JpsiKK_2017': { # LHCb-PAPER-2017-008, 1704.08217
                             # efficiency taken from LHCb-ANA-2017-006
        'label': 'LHCb $B_s^0\\to J\\!/\\!\\psi K^+K^-$ 2017',
        'omega': omega(tagging_power=0.0386, efficiency=0.6866),
        'eff': 0.6866,
    },
    # LHCb-PAPER-2016-037
    # R. Aaij et al. (The LHCb collaboration). ‘Measurement of 𝐶𝑃 violation in 𝐵0 → 𝐷+𝐷− decays’. In: (Aug. 2016). arXiv: 1608.06620.
    'LHCb_B2DD': {
        'label': 'LHCb $B^0\\to D^+D^-$',
        'omega': 0.348,
        'eff': 0.876,
        'offset': (0.015, 0),
        'yield': 1610,
        'lumi': 3,
    },
    'LHCb_Bs2Psi2SPhi': { # LHCb-PAPER-2016-027, 1608.04855
        'label': r'LHCb $B_s^0\to \psi(2S)\phi$',
        'omega': omega(tagging_power=0.0388, efficiency=0.6562),
        'eff': 0.6562,  # taken from LHCb-ANA-2013-015
    },
    # LHCb-ANA-2013-011
    'LHCb_Bs2JpsiKS': {
        'label': r'LHCb $B_s^0\to J\!/\!\psi K_S^0$',
        'omega': 0.38,  # only reported for individual samples
        'eff': 0.66,    # "
        'lumi': 3,
        'yield': 908,
    },
    # LHCb-PAPER-2014-059
    'LHCb_Bs2JpsiKK_2014': { },
    # LHCb-PAPER-2014-058
    'LHCb_Bd2JpsiPiPi_2014': { },
    # LHCb-PAPER-2014-038
    'LHCb_Bs2DsK_2014': { },
    # LHCb-PAPER-2014-026
    'LHCb_Bs2PhiPhi_2011': { },
    # R. Aaij et al. (The LHCb collaboration). ‘Measurement of 𝐶𝑃 Violation in 𝐵0 → 𝐽/𝜓𝐾S0 Decays’. In: Phys. Rev. Lett. 115 (July 2015), p. 031601. doi: 10.1103/PhysRevLett.115.031601.
    'LHCb_Bd2JpsiKS': {
        'label': 'LHCb $B^0\\to J\\!/\\!\\psi K_S^0$',
        'omega': 0.3562,
        'eff': 0.3654,
        'offset': (0.015, 0),
        'contour': (0.35, 0.5),
    },
    'LHCb_B2DstD_Run1': {
        'omega': omega(efficiency=0.802, tagging_power=0.0561),
        'eff': 0.802,
        'offset': (-0.15, 0),
    },
    'contours': {
        0.01: (0.2, 0.005),
        0.1: (0.15, 0.2),
        0.2: (0.11, 0.35),
        0.4: (0.08, 0.5),
        0.6: (0.05, 0.7),
        0.8: (0.02, 0.9),
    }
}
