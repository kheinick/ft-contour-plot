from plot_ft_contours import omega

# The first dict in the file will be used to fill the contour plot
CLASSIC_TAGGERS = {
    'mu': {
        'label': r'OS $\mu$',
        'omega': 0.30911,
        'eff': 0.09664,
        'offset': [-0.015, 0.04],
        'contour': (0.22, 0.08),
        'ref': 'LHCb-ANA-2018-021',
    },
    'vtx_ch': {
        'label': 'OS $Q_\\mathrm{vtx}$',
        'omega': 0.34751,
        'eff': 0.20597,
        'offset': [-0.05, 0.035],
        'ref': 'LHCb-ANA-2018-021',
    },
    'e': {
        'label': 'OS $e$',
        'omega': 0.33577,
        'eff': 0.04590,
        'offset': [0.015, 0],
        'contour': (0.4, 0.09),
        'ref': 'LHCb-ANA-2018-021',
    },
    'K': {
        'label': 'OS $K$',
        'omega': 0.36918,
        'eff': 0.20185,
        'ref': 'LHCb-ANA-2018-021',
    },
    'charm': {
        'label': 'OS $c$',
        'omega': 0.32581,
        'eff': 0.05500,
        'offset': [-0.045, 0],
        'ref': 'LHCb-ANA-2018-021',
    },
    'ss_k': {
        'label': 'SS $K$',
        'omega': omega(tagging_power=0.02525, efficiency=0.692),
        'eff': 0.692,
        'offset': [0.015, 0.015],
        'contour': (0.39, 0.6),
        'ref': 'LHCb-ANA-2018-025',
    },
    'ss_p': {
        'label': 'SS $p$',
        'omega': omega(tagging_power=0.00523, efficiency=0.3840),
        'eff': 0.3840,
        'offset': [0.015, -0.015],
        'ref': 'LHCb-ANA-2018-025',
    },
    'ss_pi': {
        'label': r'SS $\pi$',
        'omega': omega(tagging_power=0.0142, efficiency=0.8007),
        'eff': 0.8007,
        'ref': 'LHCb-ANA-2018-025',
    },
    'plot_text': {
        'text': (
            'Tagging algorithm performance from\n'
            r'$B_s^0\to D_s K\pi\pi$ and $B_{(s)}^0\to \pi\pi(KK)$ Run 2 data'
        ),
        'position': (0.02, 0.98),
        'hack': 70,  # this is annoying
    },
    'contours': {
        0.04: (0.18, 0.1),
        0.1: (0.15, 0.2),
        0.2: (0.11, 0.35),
        0.4: (0.08, 0.5),
        0.6: (0.05, 0.7),
        0.8: (0.02, 0.9),
    },
}
