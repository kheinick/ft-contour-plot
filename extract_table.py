#! /usr/bin/env python3
import argparse

from utils import get_config


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('config_module', type=str)
    parser.add_argument('output', type=str)
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    taggermap = get_config(args.config_module, remove_meta=True)

    with open(args.output, 'w') as of:
        for key, tagger in taggermap.items():
            of.write(
                (r'{label} & {efficiency:.1f} & {omega:.1f} & {tagging_power:.1f} & '
                 r'\cite{{{reference}}} \\').format(
                    label=tagger.get('label', key),
                    efficiency=100 * tagger['eff'],
                    omega=100 * tagger['omega'],
                    tagging_power=100 * tagger['eff'] * (1 - 2 * tagger['omega'])**2,
                    reference=tagger.get('ref', ''),
                )
                + '\n'
            )
