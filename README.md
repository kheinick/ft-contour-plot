# Flavour Tagging contour plot

Produce a contour plot in the omega-eta plane to compare different
flavour tagging algorithms.

Run `./plot_ft_contours.py --help` for some information about the available
cmdline arguments.

See `classic_taggers.py` for a map of taggers that are displayed in the plot.

Feel free to use this script for your needs, a citation of CERN-THESIS-2016-152
is very appreciated though :-)

## A comparison of LHCb Flavour Tagging algorithms
This is what you get by default.

![contours.png](./latex/figs/contours.png)

## A comparison of different Experiments performing Flavour Tagging
This plot is making use of a few more command line options. Since a few data
points define `'contour': True`, the default list of contour lines *and labels*
is adjusted in the tagger map file.
```
./plot_ft_contours.py -t tagging_comparison -o contours_comparison.png
```

![contours_comparison.png](./latex/figs/contours_comparison.png)

## LHCb-FIGURE
To build the lhcb-figure with all dependencies, run `make` from within the
`latex/` directory.
