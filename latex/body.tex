\section{Figures}
\label{sec:flavour_tagging_contour_plot}
At LHCb, time-dependent \CP violation analyses utilise numerous algorithms to
determine the initial B flavour of signal candidates.
The taggers are usually divided into same side (SS) and opposite side (OS)
taggers that aim to infer the flavour information by reconstructing the decay
products of the signal candidate hadronization companion or
the opposite side \bquark-hadron partner, respectively.

The effective tagging efficiency (or tagging power) $\varepsilon_\text{tag}$
reduces the effective size of the dataset that can be used for these
measurements.
It is defined as
\begin{equation*}
    \varepsilon_\text{eff} = \varepsilon_\text{tag} \cdot (1 - 2\omega)^2\,,
\end{equation*}
with the tagging efficiency $\varepsilon_\text{tag}$ and mistag fraction $\omega$ defined as
\begin{equation*}
    \varepsilon_\text{tag} = \frac{N_\text{tagged}}{N_\text{tagged} + N_\text{untagged}}
    \qquad\text{and}\qquad
    \omega = \frac{N_\text{false}}{N_\text{tagged}}\,,
\end{equation*}
the numbers $N_\text{tagged}$ and $N_\text{untagged}$ of tagged and untagged
events, respectively, and the number of falsely tagged events $N_\text{false}$.
The statistical uncertainty $\sigma_\text{stat}$ of flavour-tagged analyses typically scales like
\begin{equation*}
    \sigma_\text{stat} \propto \frac{1}{\sqrt{\varepsilon_\text{eff}}}\,,
\end{equation*}
making even small improvements of the flavour tagging algorithms valuable.

\autoref{fig:ft_contours} displays the performances of individual flavour
tagging algorithms that are commonly used at LHCb.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.9\linewidth]{figs/contours.pdf}
    \caption{
        The effective tagging efficiency $\varepsilon_\text{tag}$ for different
        LHCb flavour tagging algorithms is displayed in the
        $\omega$-$\varepsilon_\text{tag}$-plane.
        The numbers for the OS~taggers are extracted from the
        $\Bs\to\D_\squark\kaon\pion\pion$ measurement, referring the Run 2 data
        samples up until 2017 \cite{LHCb-ANA-2018-021}, while the numbers for
        the SS~taggers are extracted from the $\B_\text{(s)}\to \pion\pion
        (\kaon\kaon)$ measurement, including Run 2 data up until 2016
        \cite{LHCb-ANA-2018-025}.
        Different contours of equal effective tagging efficiency values are
        displayed.
    }
    \label{fig:ft_contours}
\end{figure}
The standard combination for strange \B mesons contains the OS~muon, OS
electron, OS~charm, OS~vertex charge ($Q_\text{vtx}$), and SS~kaon taggers.
In the standard combination for \Bd mesons, the SS~kaon tagger is replaced by
the SS~proton and SS~pion taggers.
The reported numbers for the OS algorithms are based on the measurement of \CP
violation in $\Bs\to\D_\squark\kaon\pion\pion$ decays, using Run 2 data from
2015 until 2017 \cite{LHCb-ANA-2018-021}, while the numbers for the SS
algorithms are taken from the measurement of \CP~violation in
$\B_\text{(s)}\to \pion\pion (\kaon\kaon)$ decays based on Run 2 data until
2016 \cite{LHCb-ANA-2018-025}.

When presenting this figure, please emphasize the fact that the
flavour tagging efficiency $\varepsilon_\text{tag}$ depends on the selection
that is applied to the dataset.
Hence the flavour tagging performance of different algorithms is not expected
to be comparable, if different offline selections were applied.

\autoref{fig:ft_contours_comparison} displays the performances of combined
flavour tagged analyses across different experiments.
The clear hierarchy between the different LHCb measurements originates from the
stricter offline selection requirements that are needed for datasets with
multiple hadrons in the final state.
As mentioned before, the tagging efficiency $\varepsilon_\text{tag}$ is usually
higher after these selections.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.9\linewidth]{figs/contours_comparison.pdf}
    \caption{
        The total effective tagging efficiency $\varepsilon_\text{tag}$ for
        multiple published analyses across different experiments is shown in
        the $\omega$-$\varepsilon_\text{tag}$-plane \cite{LHCB-PAPER-2016-037,
        LHCb-PAPER-2019-013, LHCb-PAPER-2015-004, atlas-bs2jpsiphi-2020,
    cms-bs2jpsiphi, babar, belle}.
        Different contours of equal effective tagging efficiency values are
        displayed.
    }
    \label{fig:ft_contours_comparison}
\end{figure}

The flavour tagging parameters that are displayed in \autoref{fig:ft_contours}
and \autoref{fig:ft_contours_comparison} are also listed in
\autoref{tab:ft_parameters}.
\begin{table}
    \centering
    \caption{
        Overview of flavour tagging parameters used in this document. Due to
        different conventions to report the parameters, only the nominal values
        rounded to one decimal point are reported.
    }
    \label{tab:ft_parameters}
    \begin{tabular}{l S[table-format=2.1] S[table-format=2.1] S[table-format=2.1] c}
        Algorithm / Measurement & $\varepsilon_\text{tag} / \si{\percent}$ & $\omega / \si{\percent}$ & $\varepsilon_\text{eff} / \si{\percent}$ & Reference \\
        \toprule
        \input{build/table_algorithms.tex}
        \midrule
        \input{build/table_comparison.tex}
        \bottomrule
    \end{tabular}
\end{table}
