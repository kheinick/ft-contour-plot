import importlib


def get_config(taggermap, remove_meta=False):
    taggermap = importlib.import_module('configs.' + taggermap)

    taggers = getattr(taggermap, dir(taggermap)[0])

    if remove_meta:
        taggers.pop('plot_text', None)
        taggers.pop('contours', None)

    return taggers
