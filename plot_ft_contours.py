#! /usr/bin/env python3
"""Flavour Tagging contour plot

Produce a contour plot in the omega-efficiency plane to compare different
flavour tagging algorithms.
Run `./plot_ft_contours.py --help` for some information about the available
cmdline arguments.
See `classic_taggers.py` for a map of taggers that are displayed in the plot.

Feel free to use this script for your needs, a citation of CERN-THESIS-2016-152
is very appreciated though :-)
"""

__author__ = 'Kevin Heinicke'
__email__ = 'kevin.heinicke@cern.ch'


import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.patheffects as pe
import numpy as np
from scipy.interpolate import griddata

import argparse
import os

from utils import get_config

from matplotlib import rc
rc('text', usetex=True)
rc('text.latex', preamble=r'''
\usepackage{sansmath}
\sansmath
''')


def get_available_configs():
    return [
        c.replace('.py', '')
        for c in os.listdir('configs/')
        if c not in ['__pycache__']
    ]


def parse_args():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument('-t', '--taggermap', default='classic_taggers',
                        help=f'''Use tagger map from this module to fill data
                        points into the contour plot. Defaults to
                        `classic_taggers`. Module is imported from the
                        `configs/` directory. Available configs:
                        {get_available_configs()}''')
    parser.add_argument('-o', '--output', default='contours.pdf',
                        help='''Output filename, default is `countours.pdf`.'''
                        )
    parser.add_argument('--label', default='LHCb preliminary', help='''Plot
                        label in the upper right corner, defaults to "LHCb
                        unofficial".''')
    parser.add_argument('--no-grid', default=False, action='store_true', help='''
                        Activate a plot grid.''')
    parser.add_argument('--contours', default=[], type=float, nargs='+',
                        help=f'''Set contour lines.  Defaults to no contours
                        or contours given in tagger config.''')
    parser.add_argument('--contour-labels', default=[],
                        type=str, nargs='+', help='''Set contour label
                        positions.  Unfortunately the matplotlib algorithm is
                        not able to find good positions for these. The labels
                        need to be placed "manually" in coordinates of the
                        plot. Defaults to none or the ones defined in the given
                        tagger config. Length of pairs must match length of
                        contours.''')
    parser.add_argument('--manual-contours', default=False,
                        action='store_true', help='''Interactively set contour
                        label positions''')
    return parser.parse_args()


def tagging_power(epsilon, omega):
    return epsilon * (1 - 2 * omega)**2


def omega(dilution=None, tagging_power=None, efficiency=None, dilution_squared=None):
    if dilution is not None:
        if dilution_squared is not None:
            raise ConfigurationError(
                'Parameters "dilution" and "dilution_squared" are ambiguous. '
                'Please only use one of them.'
            )
        return (1 - dilution) / 2
    elif dilution_squared is not None:
        return (1 - np.sqrt(dilution_squared)) / 2

    if tagging_power is not None and efficiency is not None:
        # thats a bit cumbersome but okay...
        return (1 - np.sqrt(tagging_power / efficiency)) / 2

    raise ConfigurationError('omega cannot be determined from given parameters')


if __name__ == '__main__':
    args = parse_args()

    taggers = get_config(args.taggermap)

    plot_text = taggers.pop('plot_text', None)

    contour_dict = {
        value: tuple(float(i) for i in label_pos.split(','))
        for value, label_pos
        in zip(args.contours, args.contour_labels)
    }
    # make sure the contour dict is removed from config
    config_contour_dict = taggers.pop('contours', {})
    if not contour_dict:
        contour_dict = config_contour_dict

    omegas = np.linspace(0, 0.5, 1000)
    epsilons = np.linspace(0, 1, 1000)
    X, Y = np.meshgrid(omegas, epsilons)
    Z = tagging_power(Y, X)

    plt.figure(figsize=(9, 6))
    im = plt.imshow(Z, extent=(0, 0.5, 0, 1), cmap=cm.viridis, aspect=0.5,
                    origin='lower')

    cbar = plt.colorbar(im)
    cbar.set_label(r'Effective tagging efficiency, $\varepsilon_\mathrm{eff}$',
                   fontsize=14)
    levels, labels = list(contour_dict.keys()), list(contour_dict.values())
    sorted_levels = sorted(levels)
    sorted_labels = [labels[i] for i in np.argsort(levels)]
    cons = plt.contour(X, Y, Z, levels=levels, colors='white')
    plt.clabel(cons, sorted_levels,
               fmt={l: '{:2.0f}\\%'.format(l * 100) for l in sorted_levels},
               manual=args.manual_contours or sorted_labels or False)
    if not args.no_grid:
        plt.grid(linewidth=0.1, color='white')
    for name, tagger in taggers.items():
        x, y = tagger['omega'], tagger['eff']
        if (contour_position := tagger.get('contour')):
            levels = [tagging_power(tagger['eff'], tagger['omega'])]
            con = plt.contour(X, Y, Z, levels=levels, colors='white')
            if type(contour_position) != tuple:
                contour_position = (x, y - 0.1)
            plt.clabel(
                con,
                levels,
                fmt={l: '{:2.1f}\\%'.format(l * 100) for l in levels},
                manual=args.manual_contours or [contour_position]
            )
        plt.plot(x, y, 'wo', clip_on=False, zorder=100, markeredgecolor='k', markeredgewidth=0.3)
        xoff, yoff = tagger.get('offset', (0.015, 0))
        tagger_label = tagger.get('label', name.replace('_', '\\_'))
        if tagger.get('hack'):
            plt.annotate('-' * tagger.get('hack'), (x, y), (x + xoff, y + yoff),
                         color=(1, 1, 1, 0), verticalalignment='center',
                         bbox=dict(boxstyle='round', fc=(1, 1, 1, 0.8), ec='w'),
                         )
            plt.text(x + xoff, y + yoff, tagger_label, va='center')
        else:
            plt.annotate(tagger_label, (x, y), (x + xoff, y + yoff),
                         color='k', verticalalignment='center',
                         bbox=dict(boxstyle='round,pad=0.4', fc=(1, 1, 1, 0.8), ec='w'),
                         )

    if args.label:
        plt.text(0.49, 0.98, args.label, horizontalalignment='right',
                 verticalalignment='top', fontsize=16, color='w',
                 path_effects=[pe.Stroke(linewidth=1.5, foreground='k'), pe.Normal()])

    if plot_text is not None:
        x, y = plot_text.get('position', (0.02, 0.98))
        # This "hack" seems to be needed if the last line of the text contains
        # latex symbols of large height which leads to a shifted bbox alignment
        # and looks aweful besides the label text
        if plot_text.get('hack') is not None:
            plt.text(x, y, '.\n.' + plot_text['hack'] * '.', ha='left',
                     color=(1, 1, 1, 0), va='top',
                     bbox=dict(boxstyle='round,pad=0.5', fc=(1, 1, 1, 0.8), ec='w'))
            plt.text(x, y, plot_text['text'], ha='left', va='top')
        else:
            plt.text(x, y, plot_text['text'], ha='left', va='top',
                     bbox=dict(boxstyle='round,pad=0.4', fc='w'))

    plt.xlabel(r'Mistag fraction, $\omega$', fontsize=14)
    plt.ylabel(r'Tagging efficiency, $\varepsilon_\mathrm{tag}$', fontsize=14)
    plt.tight_layout()
    plt.savefig(args.output, bbox_inches='tight')
